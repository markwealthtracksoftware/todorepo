//
//  AddTaskTableViewController.h
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasks.h"
#import "TaskListViewController.h"

@class TaskListViewController;

@interface AddTaskTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITextField *namefield;
@property (strong, nonatomic) TaskListViewController *tasklistviewcontroller;

-(IBAction)cancelbutton:(id)sender;
-(IBAction)donebutton:(id)sender;

@end
