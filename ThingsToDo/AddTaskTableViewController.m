//
//  AddTaskTableViewController.m
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import "AddTaskTableViewController.h"

@interface AddTaskTableViewController ()

@end

@implementation AddTaskTableViewController


@synthesize namefield=_namefield;
@synthesize tasklistviewcontroller;

-(IBAction)cancelbutton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:NO];
}
-(IBAction)donebutton:(id)sender{
    
    Tasks *newtask = [[Tasks alloc] initWithName:self.namefield.text done:NO];
    [self.tasklistviewcontroller.tasks addObject:newtask];
    
    //IF YOU DONT CREATE THE INSTANCE PROPERTY IN .h then you must call the old way 
    //TaskListViewController *iu=[[TaskListViewController alloc] init];
    //[iu.tasks addObject:newtask];
    
    [self dismissViewControllerAnimated:YES completion:NO];
    
    [self.tasklistviewcontroller.tableView reloadData];
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
