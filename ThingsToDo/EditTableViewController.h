//
//  EditTableViewController.h
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasks.h"

@class Tasks;

@interface EditTableViewController : UITableViewController

@property (nonatomic,strong) IBOutlet UITextField *namefield;
@property (nonatomic, strong) IBOutlet UISwitch *doneswitch;
@property (nonatomic, strong) Tasks *tasks;

-(IBAction)edittaskdata:(id)sender;

@end
