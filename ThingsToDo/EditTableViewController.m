//
//  EditTableViewController.m
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import "EditTableViewController.h"

@interface EditTableViewController ()

@end

@implementation EditTableViewController

@synthesize namefield;
@synthesize doneswitch;
@synthesize tasks=_tasks;


-(IBAction)edittaskdata:(id)sender{
    self.tasks.name = namefield.text;
    self.tasks.done = doneswitch.isOn;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    namefield.text=self.tasks.name;
    [self.doneswitch setOn:self.tasks.done];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
