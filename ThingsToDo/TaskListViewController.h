//
//  TaskListViewController.h
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasks.h"
#import "AddTaskTableViewController.h"
#import "EditTableViewController.h"

@interface TaskListViewController : UITableViewController

@property (nonatomic, strong)NSMutableArray *tasks;

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

-(IBAction)editbuttonpressed:(id)sender;

@end
