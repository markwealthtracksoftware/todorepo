//
//  TaskListViewController.m
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import "TaskListViewController.h"

@interface TaskListViewController ()

@end

@implementation TaskListViewController

@synthesize tasks;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"AddTaskSegue"]){
        UINavigationController *navcon=segue.destinationViewController;
        AddTaskTableViewController *addtasktableviewcontroller=[navcon.viewControllers objectAtIndex:0];
        addtasktableviewcontroller.tasklistviewcontroller=self;
            }
        
        else
            if([segue.identifier isEqualToString:@"EditDonesegue"]||[segue.identifier isEqualToString:@"EditNotDonesegue"]){
                EditTableViewController *edittableviewcontroller=segue.destinationViewController;
                edittableviewcontroller.tasks= [self.tasks objectAtIndex:self.tableView.indexPathForSelectedRow.row];
            }
}


-(IBAction)editbuttonpressed:(id)sender{
    self.editing=!self.editing;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData]; //***********
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *joop = @"defefef";
    joop=[joop stringByAppendingString:@" - appending"];
    NSLog(@"%@",joop);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    self.tasks = [[NSMutableArray alloc] init];
    
    Tasks *task=[[Tasks alloc] initWithName:@"Sign off on NCT for the Millenium Falcon" done:NO];
    
    [self.tasks addObject:task];
    
    Tasks *donetask = [[Tasks alloc] initWithName:@"decomission the USS Enterprise after Lunch" done:YES];
    
    [self.tasks addObject:donetask];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return self.tasks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *NotDoneCellIdentifier = @"NotDoneTask";
    static NSString *DoneCellIdentifier = @"DoneTask";
    
    Tasks *currenttask = [self.tasks objectAtIndex:indexPath.row]; //****
        
    //NSString *cellIdentifier=currenttask.done ? DoneCellIdentifier : NotDoneCellIdentifier;
    NSString *cellIdentifier;
    if(currenttask.done)
        cellIdentifier=DoneCellIdentifier;
            else
                cellIdentifier=NotDoneCellIdentifier;

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    // Configure the cell...
    cell.textLabel.text=currenttask.name; //****
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.tasks removeObjectAtIndex:indexPath.row]; //**************
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    Tasks *movetask=[self.tasks objectAtIndex:fromIndexPath.row];
    [self.tasks removeObjectAtIndex:fromIndexPath.row];
    [self.tasks insertObject:movetask atIndex:toIndexPath.row];
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
        
    
    return YES;
}

//changes to test commit in sourcetree
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
