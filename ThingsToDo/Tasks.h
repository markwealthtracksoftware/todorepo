//
//  Tasks.h
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tasks : NSObject

@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) BOOL done;

-(id)initWithName:(NSString *)name done:(BOOL)done;


@end
