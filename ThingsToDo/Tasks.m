//
//  Tasks.m
//  ThingsToDo
//
//  Created by Mark Jones on 27/11/2013.
//  Copyright (c) 2013 Mark Jones. All rights reserved.
//

#import "Tasks.h"

@implementation Tasks

@synthesize name=_name;
@synthesize done=_done;

-(id)initWithName:(NSString *)name done:(BOOL)done{
    
    self = [super init];
    
    if(self){
        self.name=name;
        self.done=done;
    }
    return self;
}

@end
